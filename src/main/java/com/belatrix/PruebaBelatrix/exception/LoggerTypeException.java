package com.belatrix.PruebaBelatrix.exception;

public class LoggerTypeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public LoggerTypeException(String message) {
        super(message);
    }

}
