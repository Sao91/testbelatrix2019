package com.belatrix.PruebaBelatrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaBelatrixApplication {


	public static void main(String[] args) {
		SpringApplication.run(PruebaBelatrixApplication.class, args);
	}


}
