package com.belatrix.PruebaBelatrix.resources;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.FileHandler;

import com.belatrix.PruebaBelatrix.config.Config;
import com.belatrix.PruebaBelatrix.exception.HandlerException;
import com.belatrix.PruebaBelatrix.exception.LogFileException;

public class FileManager implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Config config;
	
	public FileManager(Config config) {
		super();
		this.config = config;
	}
	
	public File getLogFile() {
		File logFile = new File(this.config.getProperty("logger.logFilePath"));
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				throw new LogFileException("Error create new log file.", e);
			}
		}
		return logFile;
	}
	
	public FileHandler getFileHandler() {
		try {
			getLogFile();
			return new FileHandler(this.config.getProperty("logger.logFilePath"));
		} catch (SecurityException | IOException e) {
			throw new  HandlerException("Error get file handler.", e);
		}
	}

}
