package com.belatrix.PruebaBelatrix.resources;

import java.io.Serializable;
import java.util.logging.ConsoleHandler;

import com.belatrix.PruebaBelatrix.config.Config;

public class ConsoleManager implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static ConsoleManager instance = null;
	
	private ConsoleManager(Config configuration) {
		super();
	}
	
	public static synchronized ConsoleManager getInstance(Config configuration) {
		if (instance == null) {
			instance = new ConsoleManager(configuration);
		}
		return instance;
	}
	
	public ConsoleHandler getConsoleHandler() {
		return new ConsoleHandler();
	}

}
