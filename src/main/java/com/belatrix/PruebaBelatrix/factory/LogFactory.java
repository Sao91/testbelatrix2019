package com.belatrix.PruebaBelatrix.factory;

import com.belatrix.PruebaBelatrix.config.Config;
import com.belatrix.PruebaBelatrix.config.PropertiesConfig;
import com.belatrix.PruebaBelatrix.enums.LogType;
import com.belatrix.PruebaBelatrix.exception.LoggerTypeException;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.util.ConsoleLog;
import com.belatrix.PruebaBelatrix.util.DatabaseLog;
import com.belatrix.PruebaBelatrix.util.FileLog;

public class LogFactory {
	
	private LogFactory() {
		super();
	}
	
	public static Log getLogger(String type) {
		if (LogType.FILE.getType().equals(type)) {
			return new FileLog(new PropertiesConfig());
		} else if (LogType.CONSOLE.getType().equals(type)) {
			return new ConsoleLog(new PropertiesConfig());
		} else if (LogType.DATABASE.getType().equals(type)) {
			return new DatabaseLog(new PropertiesConfig());
		} else {
			throw new LoggerTypeException("Invalid configuration | Logger type not valid!");
		}
	}
	
	public static Log getLogger(String type, Config configuration) {
		if (LogType.FILE.getType().equals(type)) {
			return new FileLog(configuration);
		} else if (LogType.CONSOLE.getType().equals(type)) {
			return new ConsoleLog(configuration);
		} else if (LogType.DATABASE.getType().equals(type)) {
			return new DatabaseLog(configuration);
		} else {
			throw new LoggerTypeException("Logger type not valid!");
		}
	}

}
