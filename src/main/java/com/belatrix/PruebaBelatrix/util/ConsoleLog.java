package com.belatrix.PruebaBelatrix.util;

import java.util.logging.Level;

import org.apache.logging.log4j.util.Strings;

import com.belatrix.PruebaBelatrix.config.Config;
import com.belatrix.PruebaBelatrix.exception.MessageException;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.ConsoleManager;

public class ConsoleLog implements Log {
	
	private ConsoleManager manager;
	
	public ConsoleLog(Config configuration) {
		this.manager = ConsoleManager.getInstance(configuration);
		logger.addHandler(this.manager.getConsoleHandler());
	}
	
	public void addMessage(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Message must be specified");
		}
		logger.log(Level.INFO, message);
	}

	public void addWarning(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Message must be specified");
		}
		logger.log(Level.WARNING, message);
	}

	public void addError(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Error must be specified");
		}
		logger.log(Level.SEVERE, message);
	}

}