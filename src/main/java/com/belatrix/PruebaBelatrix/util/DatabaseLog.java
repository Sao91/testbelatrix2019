package com.belatrix.PruebaBelatrix.util;

import java.text.DateFormat;
import java.util.Date;

import org.apache.logging.log4j.util.Strings;

import com.belatrix.PruebaBelatrix.config.Config;
import com.belatrix.PruebaBelatrix.enums.LogLevel;
import com.belatrix.PruebaBelatrix.exception.MessageException;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.DatabaseManager;



public class DatabaseLog implements Log {
	
	private DatabaseManager manager;
	
	public DatabaseLog(Config configuration) {
		this.manager = DatabaseManager.getInstance(configuration);
	}
	
	public void addMessage(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Message must be specified");
		}
		String errorMessage = String.format("message %s %s",  DateFormat.getDateInstance(DateFormat.LONG).format(new Date()),message);
		this.manager.insertMessageBD(errorMessage, LogLevel.MESSAGE.getId());
	}

	public void addWarning(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Warning must be specified");
		}
		String errorMessage = String.format("warning %s %s",  DateFormat.getDateInstance(DateFormat.LONG).format(new Date()),message);
		this.manager.insertMessageBD(errorMessage, LogLevel.WARNING.getId());
	}

	public void addError(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Error must be specified");
		}
		String errorMessage =String.format("error %s %s",  DateFormat.getDateInstance(DateFormat.LONG).format(new Date()),message);
		this.manager.insertMessageBD(errorMessage, LogLevel.ERROR.getId());
	}

}