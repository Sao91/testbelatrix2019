package com.belatrix.PruebaBelatrix.util;

import java.util.logging.Level;

import org.apache.logging.log4j.util.Strings;

import com.belatrix.PruebaBelatrix.config.Config;
import com.belatrix.PruebaBelatrix.exception.MessageException;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.FileManager;

public class FileLog implements Log {
	
	private FileManager manager;
	
	public FileLog(Config configuration) {
		this.manager = new FileManager(configuration);
		logger.addHandler(this.manager.getFileHandler());
	}
	
	public void addMessage(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Message must be specified");
		}
		logger.log(Level.INFO, message);
	}

	public void addWarning(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Warning must be specified");
		}
		logger.log(Level.WARNING, message);
	}

	public void addError(String message) {
		if (Strings.isBlank(message)) {
			throw new MessageException("Error must be specified");
		}
		logger.log(Level.SEVERE, message);
	}

}
