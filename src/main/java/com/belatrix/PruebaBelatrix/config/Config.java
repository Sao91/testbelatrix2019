package com.belatrix.PruebaBelatrix.config;

import java.io.Serializable;

public abstract class Config implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public abstract String getProperty(final String property);

}
