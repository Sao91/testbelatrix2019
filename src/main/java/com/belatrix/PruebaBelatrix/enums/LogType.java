package com.belatrix.PruebaBelatrix.enums;

public enum LogType {
	
	CONSOLE ("console"), 
	DATABASE ("database"), 
	FILE ("file");
	
	private String type;
	
	private LogType(final String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	

}
