package com.belatrix.PruebaBelatrix.enums;

public enum LogLevel {
	
	MESSAGE (1, "message"), 
	ERROR (2, "error"),
	WARNING (3, "warning");
	
	private int id;
	private String name;
	
	private LogLevel(final int id, final String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

}
