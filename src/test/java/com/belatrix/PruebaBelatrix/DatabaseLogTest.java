package com.belatrix.PruebaBelatrix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.belatrix.PruebaBelatrix.config.PropertiesConfig;
import com.belatrix.PruebaBelatrix.enums.LogType;
import com.belatrix.PruebaBelatrix.enums.LogLevel;
import com.belatrix.PruebaBelatrix.factory.LogFactory;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.DatabaseManager;
import com.belatrix.PruebaBelatrix.util.DatabaseLog;

public class DatabaseLogTest {
	
	@Before
	public final void baseSetUp() {}
	
	@After
	public final void baseTearDown() {}
	
	@Test
    public void testLoggerFactory_DatabaseType() {
		Log logger = LogFactory.getLogger(LogType.DATABASE.getType());
        assertEquals(true, logger instanceof DatabaseLog);
    }
	
	@Test
    public void testDatabaseManager_BDConnectionValid() {
		DatabaseManager bdManager = DatabaseManager.getInstance(new PropertiesConfig());
		try {
			assertTrue(bdManager.getConnection().isValid(0));
		} catch (SQLException e) {
			assertTrue(false);
		}
    }
	
	@Test
    public void insertMessage() {
		try {
			DatabaseManager db = DatabaseManager.getInstance(new PropertiesConfig());
			db.createLogTable();
			db.insertMessageBD("Test insert message DB", LogLevel.MESSAGE.getId());
			assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
    }
	
	@Test
    public void addLog() {
		try {
			Log log = LogFactory.getLogger(LogType.DATABASE.getType());
			log.addMessage("Test Multiple - Message Info!");
			log.addWarning("Test Multiple - Message Warning!");
	        assertTrue(true);
		} catch (Exception e) {
			assertTrue(false);
		}
    }
	
}
