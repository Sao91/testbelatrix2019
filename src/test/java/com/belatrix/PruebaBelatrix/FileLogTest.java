package com.belatrix.PruebaBelatrix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.belatrix.PruebaBelatrix.config.PropertiesConfig;
import com.belatrix.PruebaBelatrix.enums.LogType;
import com.belatrix.PruebaBelatrix.factory.LogFactory;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.FileManager;
import com.belatrix.PruebaBelatrix.util.FileLog;

public class FileLogTest {
	
	@Before
	public final void baseSetUp() {}
	
	@After
	public final void baseTearDown() {}
	
	@Test
    public void testLoggerFactory_FileType() {
		Log log = LogFactory.getLogger(LogType.FILE.getType());
        assertEquals(true, log instanceof FileLog);
    }
	
	
	@Test
    public void testFileManager_HandlerValid() {
		FileManager fileManager = new FileManager(new PropertiesConfig());
		assertNotNull(fileManager.getFileHandler());
    }
	
	@Test
    public void addMessageInfo() {
		Log log = LogFactory.getLogger(LogType.FILE.getType());
		log.addMessage("Test - Message Info ");
        assertEquals(true, log instanceof FileLog);
    }
	
	@Test
    public void addMultipleMessage() {
		Log log = LogFactory.getLogger(LogType.FILE.getType());
		log.addMessage("Test Multiple - Message Info!");
		log.addWarning("Test Multiple - Message Warning!");
        assertEquals(true, log instanceof FileLog);
    }
	
}
