package com.belatrix.PruebaBelatrix;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.belatrix.PruebaBelatrix.config.PropertiesConfig;
import com.belatrix.PruebaBelatrix.enums.LogType;
import com.belatrix.PruebaBelatrix.factory.LogFactory;
import com.belatrix.PruebaBelatrix.interfaces.Log;
import com.belatrix.PruebaBelatrix.resources.ConsoleManager;
import com.belatrix.PruebaBelatrix.util.ConsoleLog;

public class ConsoleLogTest {
	
	@Before
	public final void baseSetUp() {}
	
	@After
	public final void baseTearDown() {}
	
	@Test
    public void testLoggerFactory_ConsoleType() {
		Log logger = LogFactory.getLogger(LogType.CONSOLE.getType());
        assertEquals(true, logger instanceof ConsoleLog);
    }
	
	@Test
    public void testConsoleManager_HandlerValid() {
		ConsoleManager consoleManager = ConsoleManager.getInstance(new PropertiesConfig());
		assertNotNull(consoleManager.getConsoleHandler());
    }
	
	@Test
    public void addMessageInfo() {
		Log log = LogFactory.getLogger(LogType.CONSOLE.getType());
		log.addMessage("Test Console - Message Info ");
        assertEquals(true, log instanceof ConsoleLog);
    }
	
	@Test
    public void addMultipleMessage() {
		Log log = LogFactory.getLogger(LogType.CONSOLE.getType());
		log.addMessage("Test Multiple Console - Message Info!");
		log.addWarning("Test Multiple Console - Message Warning!");
		log.addError("Test Multiple Console - Message Error!");
        assertEquals(true, log instanceof ConsoleLog);
    }
	
}
