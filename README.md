#Ejercicio de revisión de código / refactorización

Revise el siguiente fragmento de código. Suponga que todos los conjuntos referenciados se han incluido correctamente.

El código se utiliza para registrar diferentes mensajes en una aplicación. Queremos poder iniciar sesión en un archivo de texto, la consola y / o la base de datos. Los mensajes se pueden marcar como mensaje, advertencia o error. También queremos la capacidad de poder seleccionar selectivamente lo que se registra, como poder registrar solo errores o solo errores y advertencias.

Si revisara el siguiente código, ¿qué comentarios le daría? Sea específico e indique cualquier error que pueda ocurrir, así como otras mejores prácticas y refactorización de código que se deben hacer.

Vuelva a escribir el código en función de los comentarios que proporcionó en la pregunta 1. Incluya pruebas unitarias en su código.


#Comentarios
El codigo no cumple con los princios SOLID:
-	S: Single responsibility principle o Principio de responsabilidad única
-	O: Open/closed principle o Principio de abierto/cerrado
-	L: Liskov substitution principle o Principio de sustitución de Liskov
-	I: Interface segregation principle o Principio de segregación de la interfaz
-	D: Dependency inversion principle o Principio de inversión de dependencia

Debido que una misma clase tiene diferentes comportamientos o responsabilidades, como conectarse a la DB, verificar la existencia de archivos, configuración de parametros entre otros incumple con el primer principio. implicando que el cambio en una funcionalidad provocará la modificación en otra.

La clase debido a la estructura que maneja no puede extenderse impidiendo ser reutilizada en un futuro por otros componentes.


El codigo presenta muchos parametros de configuración entre ellos de tipo booleano, con la caracteristica de poder ser usados en multiples condicionales sucesivamente, mostrando un codigo un poco repetitivo, que no es facil de interpretar, llegando ser el mismo inmantenible de seguir creciendo la logica en el mismo.



#Errores

- En el código se instancia una variable <code> String l=null; </code> a la cual se le concadena con el simbolo <code> + </code> en vez de usar un StringFormat, String.concat o StringBuilder, los cuales tienen un proceso de ejecucion mucho mas rapido y que en momentos de cargas con alta demanda en transacciones y uso de memoria son mas eficientes.

- La variable <code> String l=null; </code> se concadena con los mensajes de error , más la hora del servidor, pero la misma no es utilizada en ninguna parte del código, siendo innecesaria su creación, debido que no es utilizada en el comportamiento esperado. 

#Mejores prácticas y refactorizacion

- Hacer uso de los principios SOLID
- Crear Componentes desacoplados que puedan ser extendidos
- Manejo y control de exepciones
- Configuraciones parametrizables y abiertas
- Implementación del patron singleton
- Eliminar el simbolo <code> + </code> y utilizar una variante de la clase String
-Integración de pruebas unitarias.
 
<code>

	import java.io.File;
	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.Statement;
	import java.text.DateFormat;
	import java.util.Date;
	import java.util.Map;
	import java.util.Properties;
	import java.util.logging.ConsoleHandler;
	import java.util.logging.FileHandler;
	import java.util.logging.Level;
	import java.util.logging.Logger;
	
	public class JobLogger {
		private static boolean logToFile;
		private static boolean logToConsole;
		private static boolean logMessage;
		private static boolean logWarning;
		private static boolean logError;
		private static boolean logToDatabase;
		private boolean initialized;
		private static Map dbParams;
		private static Logger logger;
	
		public JobLogger(boolean logToFileParam, boolean logToConsoleParam, boolean logToDatabaseParam,
				boolean logMessageParam, boolean logWarningParam, boolean logErrorParam, Map dbParamsMap) {
			logger = Logger.getLogger("MyLog");  
			logError = logErrorParam;
			logMessage = logMessageParam;
			logWarning = logWarningParam;
			logToDatabase = logToDatabaseParam;
			logToFile = logToFileParam;
			logToConsole = logToConsoleParam;
			dbParams = dbParamsMap;
		}
	
		public static void LogMessage(String messageText, boolean message, boolean warning, boolean error) throws Exception {
			messageText.trim();
			if (messageText == null || messageText.length() == 0) {
				return;
			}
			if (!logToConsole && !logToFile && !logToDatabase) {
				throw new Exception("Invalid configuration");
			}
			if ((!logError && !logMessage && !logWarning) || (!message && !warning && !error)) {
				throw new Exception("Error or Warning or Message must be specified");
			}
	
			Connection connection = null;
			Properties connectionProps = new Properties();
			connectionProps.put("user", dbParams.get("userName"));
			connectionProps.put("password", dbParams.get("password"));
	
			connection = DriverManager.getConnection("jdbc:" + dbParams.get("dbms") + "://" + dbParams.get("serverName")
					+ ":" + dbParams.get("portNumber") + "/", connectionProps);
	
			int t = 0;
			if (message && logMessage) {
				t = 1;
			}
	
			if (error && logError) {
				t = 2;
			}
	
			if (warning && logWarning) {
				t = 3;
			}
	
			Statement stmt = connection.createStatement();
	
			String l = null;
			File logFile = new File(dbParams.get("logFileFolder") + "/logFile.txt");
			if (!logFile.exists()) {
				logFile.createNewFile();
			}
			
			FileHandler fh = new FileHandler(dbParams.get("logFileFolder") + "/logFile.txt");
			ConsoleHandler ch = new ConsoleHandler();
			
			if (error && logError) {
				l = l + "error " + DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
			}
	
			if (warning && logWarning) {
				l = l + "warning " +DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
			}
	
			if (message && logMessage) {
				l = l + "message " +DateFormat.getDateInstance(DateFormat.LONG).format(new Date()) + messageText;
			}
			
			if(logToFile) {
				logger.addHandler(fh);
				logger.log(Level.INFO, messageText);
			}
			
			if(logToConsole) {
				logger.addHandler(ch);
				logger.log(Level.INFO, messageText);
			}
			
			if(logToDatabase) {
				stmt.executeUpdate("insert into Log_Values('" + message + "', " + String.valueOf(t) + ")");
			}
		}
	}
</code>
